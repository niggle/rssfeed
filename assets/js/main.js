var RSSFeed = function() {

    var URL = undefined;
    var Element = [];


    var events = {
        //load feeds from google api
        _load: function() {

            var loadedFeed = new google.feeds.Feed(URL);

            loadedFeed.load(function(result) {
                funcs._setData(result);
            });

        },

        //load feeds from our webservice
        _loadWebservice: function(){
            $.ajax({
                type: 'GET',
                url:  'webservice.php',
                cache: 'false',
                dataType: 'json',
                data:{url: URL},
                success: function(response) {
                    funcs._setData(response);

                },
                error: function(error) {
                    funcs._setInvalid('Feed Invalid');

                }
            });

        }
      
             
    }

    var funcs = {
        // set URL
        _setURL: function(url){

            URL = url;
        },
        //Set Element
        _setElement: function(element){

            Element = element;
        },
        //Load API
        _setAPI: function(module, version){
            google.load('feeds', version, {'callback': events._load });
        },
        //render the result in page
        _setData: function(result){
            if (!result.error) {
                $('#feed').empty();
                for (var i = 0, size=result.feed.entries.length; i < size ; i++) {
                    var entry = result.feed.entries[i];
                    render = '<div class="row margin">'+
                            '<div class="col-xs-12 text-center visible-xs visible-sm">'+
                            '<img src="http://placehold.it/150x150">'+
                            '</div>'+
                            '<div class="col-xs-12 visible-xs visible-sm text-center">'+
                            '<h3> <a href="'+entry.link+'" target="_blank">'+entry.title+'</a></h3>'+
                            '<span>'+entry.contentSnippet+'</span>'+
                             '</div>'+
                             '<div class="col-xs-2 hidden-xs hidden-sm">'+
                                '<img src="http://placehold.it/150x150">'+
                             '</div>'+
                             '<div class="col-xs-10 visible-xs hidden-xs hidden-sm">'+
                                '<h3> <a href="'+entry.link+'" target="_blank">'+entry.title+'</a></h3>'+
                                '<span>'+ entry.contentSnippet +'</span>'+
                             '</div>' 
                             '</div>';

                $('#feed').append(render);
                
                    console.log(entry.title);
                    console.log(entry.link);
                    console.log(entry.contentSnippet);
                }
                if(Element){
                    funcs._enableInputs(Element);
                }
            }
            else{
                funcs._setInvalid('Feed Invalid');
            }
        },
        //check if url is valid as url
        _validateURL: function(url){

            //regex to validate url
            var expression = '(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})';
            
            return url.match(expression)
        },
        // invalid data or urls
        _setInvalid: function(message){
            if(Element){
                funcs._enableInputs(Element);
                Element[0].parent('div').addClass('has-error');
            }
            alert(message);
            
            

        },
        //disable inputs
        _disableInputs: function(){
            for(var i=0, count=Element.length; i<count; i++){
                Element[i].prop("disabled", true);
            }

        },
        //enable inputs
        _enableInputs: function(){
            for(var i=0, count=Element.length; i<count; i++){
                Element[i].prop("disabled", false);
            }
        }
    };

    // construct function, element will be an array of elements [<input>, <button>]
    this.init = function(url, element, webservice) {
        
        if(element){
            funcs._setElement(element);
        }
        

        if(funcs._validateURL(url)){
            funcs._setURL(url);

            //check to load from webservice or js google api
            if(webservice){
                events._loadWebservice(); 
            }
            else{
                funcs._setAPI('feeds', '1')
            }
        }
        else{
            funcs._setInvalid("Invalid Url");
        }

    };

};