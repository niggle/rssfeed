<?php
class RSSFeed
{		
	//url to query
	private $url = ""; 

	public function setUrl($url)
	{	
		//set url to query
		$this->url = $url;
	}

	/**
	 * load feeds connects to ajax.google.apis and load the url of class.
	 *
	*/
	private function loadFeeds()
	{
		$url = urlencode ($this->url);

		// sendRequest
		$ch = curl_init("http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&q=".$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_REFERER, "rssfeed.sagomes.net");
		$body = curl_exec($ch);
		curl_close($ch);

		return $body;

	}

	/**
	 *  get the feeds, filter and return only the responseData
	 *
	 */
	public function getFeeds()
	{
		$body = $this->loadFeeds();
		$json = json_decode($body);
		$json = $json->responseData;

		return json_encode($json);
		
	} 

}
if( $_GET['url']){

	$obj = new RSSFeed;
	$url = $_GET['url'];
	$obj->setUrl($url);
	echo $obj->getFeeds();
}




?>